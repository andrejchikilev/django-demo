from django import forms
from django.contrib.auth.models import User
from .models import Tag, Author


class AddAuthorForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.filter(author__isnull=True), label='User', empty_label='---"---')
    use_full_name_as_pen_name = forms.BooleanField(label="Use full name as pen name", initial=True, required=False)

    class Meta:
        model = Author
        fields = ['user', 'pen_name', 'use_full_name_as_pen_name']

class AddNoteForm(forms.Form):
    text = forms.CharField(max_length=2000, widget=forms.Textarea)
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(), required=False)
    is_private = forms.BooleanField(required=False, initial=True)