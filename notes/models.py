from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    pen_name = models.CharField(max_length=30, blank=True)

    @property
    def full_name(self):
        return f'{self.user.first_name} {self.user.last_name}'

    def get_pen_name(self):
        return self.pen_name if self.pen_name else f'{self.user.first_name[0]}. {self.user.last_name}'

    def __str__(self):
        return f'{self.pk}: {self.full_name}'


class Tag(models.Model):
    text = models.CharField(max_length=20)

    def __str__(self): 
        return self.text


class Note(models.Model):
    author = models.ForeignKey(Author, null=True, on_delete=models.SET_NULL, related_name='notes')
    authors_name = models.CharField(max_length=50, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    text = models.TextField()
    tags = models.ManyToManyField(Tag, blank=True)
    is_private = models.BooleanField(default=True)
    liked_by = models.ManyToManyField(Author, blank=True, related_name='liked_notes')

    def __str__(self):
        return f'{self.pk}: {self.text[:30]}...'

    def save(self, *args, **kwargs):
        if self.author:
            self.authors_name = self.author.full_name
        super().save(*args, **kwargs)

    @property
    def likes_count(self):
        return self.liked_by.count()

    def toggle_private(self):
        self.is_private = not self.is_private

    def get_absolute_url(self):
        return reverse('note-detail', args=[self.pk])


class Comment(models.Model):
    author = models.ForeignKey(Author, null=True, on_delete=models.SET_NULL)
    authors_name = models.CharField(max_length=50, blank=True)
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    note = models.ForeignKey(Note, null=True, blank=True, on_delete=models.CASCADE)
    parent_comment = models.ForeignKey('Comment', null=True, blank=True, on_delete=models.CASCADE, related_name='sub_comments')

    def __str__(self):
        return f'{self.pk}: {self.text[:30]}...'

    def clean(self):
        if self.note and self.parent_comment:
            raise ValidationError('The comment should point to a note or another comment but not both')
        if not (self.note or self.parent_comment):
            raise ValidationError('The comment should point to a note or another comment')

    def save(self, *args, **kwargs):
        if self.author:
            self.authors_name = self.author.full_name
        super().save(*args, **kwargs)