from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('authors/', views.AuthorIndexView.as_view(), name='authors-index'),
    path('authors/<int:id>/', views.AuthorDetailView.as_view(extra_context={'additional_var': 5}), name='author-detail'),
    path('notes/<int:note_id>/', views.note_detail, name='note-detail'),
    path('notes/add/', views.AddNote.as_view(), name='note-add'),
    path('notes/<int:pk>/edit/', views.EditNote.as_view(), name='note-edit'),
    path('notes/<int:pk>/delete/', views.DeleteNote.as_view(), name='note-edit'),
]