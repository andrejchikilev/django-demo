from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse
from .models import Author, Note
from .forms import AddAuthorForm, AddNoteForm


def index(request):
    return HttpResponseRedirect(reverse('authors-index'))


class AuthorIndexView(TemplateView):
    template_name = 'notes/authors_index.html'
    http_method_names = ['get', 'post']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Authors'
        context['author_list'] = Author.objects.all()
        context['form'] = AddAuthorForm()
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = AddAuthorForm(request.POST)
        if form.is_valid():
            new_author = form.save(commit=False)
            if form.cleaned_data.get('use_full_name_as_pen_name'):
                new_author.pen_name = f'{new_author.user.first_name[0]}. {new_author.user.last_name}'
            new_author.save()
        return self.render_to_response(context)


# class AuthorIndexView(ListView):
#     template_name = 'notes/authors_index.html'
#     model = Author
#     queryset = Author.objects.all()

# class AuthorDetailView(TemplateView):
#     template_name = 'notes/author_detail.html'

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         author = Author.objects.get(pk=kwargs.get('author_id'))
#         context.update({
#             'title': author.full_name,
#             'author': author,
#         })
#         return context

#     def get(self, request, **kwargs):
#         context = self.get_context_data(**kwargs)
#         if request.user == context.get('author').user:
#             form = AddNoteForm()
#             context['form'] = form
#         return self.render_to_response(context)

#     def post(self, request, **kwargs):
#         context = self.get_context_data(**kwargs)
#         form = AddNoteForm(self.request.POST)
#         if form.is_valid():
#             new_note = Note(
#                 author=context.get('author'),
#                 text=form.cleaned_data.get('text'),
#                 is_private=form.cleaned_data.get('is_private'),
#             )
#             new_note.save()
#             new_note.tags.set(form.cleaned_data.get('tags')),
#             context['form'] = AddNoteForm()
#         return self.render_to_response(context)


class AuthorDetailView(DetailView):
    model = Author
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super().get_context_data(**kwargs)
        context['title'] = self.object.full_name
        return context


class AddNote(CreateView):
    model = Note
    fields = '__all__'
    template_name = 'notes/add_note.html'


class EditNote(UpdateView):
    model = Note
    fields = '__all__'
    template_name = 'notes/add_note.html'


class DeleteNote(DeleteView):
    model = Note
    success_url = '/notes/'


def note_detail(request, note_id):
    note = Note.objects.get(pk=note_id)
    comments = note.comment_set.all().order_by('-created')
    context = {
        'title': f'{note.text[:30]}',
        'note': note,
        'comments': comments,
    }
    return render(request, 'notes/note_detail.html', context)