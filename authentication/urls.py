from django.urls import path
from django.contrib.auth.views import LogoutView

from . import views

urlpatterns = [
    path('register', views.RegistrationView.as_view(), name='register'),
    path('login', views.AuthView.as_view(), name='custom_login'),
    path('logout', LogoutView.as_view(), name='custom_logout'),
]