from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User

from notes.models import Author


@receiver(post_save, sender=User)
def create_author(sender, instance, created, *args, **kwargs):
    if instance and created:
        author = Author(user=instance)
        author.pen_name = author.get_pen_name()
        author.save()