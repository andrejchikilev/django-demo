from django.views.generic.base import TemplateView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from .forms import RegisterForm


class RegistrationView(TemplateView):
    template_name = 'authentication/form.html'
    form_class = RegisterForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Registration',
            'button_text': 'Register',
            'form': self.form_class()
        })
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            context['message'] = 'User created succesfully'
        return self.render_to_response(context)


class AuthView(LoginView):
    template_name = 'authentication/login_form.html'