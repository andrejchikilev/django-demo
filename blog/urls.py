from django.urls import path

from . import views

urlpatterns = [
    path('authors/', views.AuthorList.as_view()),
    path('articles/', views.ArticleList.as_view()),
]