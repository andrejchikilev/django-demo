# Generated by Django 3.0 on 2019-12-22 10:43

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_model2_birthday'),
    ]

    operations = [
        migrations.CreateModel(
            name='Model3',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
            ],
        ),
        migrations.AddField(
            model_name='model2',
            name='url',
            field=models.URLField(blank=True, max_length=300, null=True),
        ),
    ]
