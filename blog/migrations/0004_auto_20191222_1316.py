# Generated by Django 3.0 on 2019-12-22 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_uselessmodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uselessmodel',
            name='language',
            field=models.CharField(choices=[('ru', 'Russian'), ('en', 'English'), ('by', 'Belarussian')], default='ru', help_text='Field for chosing language', max_length=2),
        ),
    ]
