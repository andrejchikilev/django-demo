from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.views.generic import ListView
from django.views.generic.base import TemplateView

from .models import Author, Article

# def index(request):
#     return HttpResponse("<html><head></head><body>Hello, world. You're at the <strong>blog index.</strong></body>")


class AuthorList(ListView):
    model = Author


class ArticleList(ListView):
    model = Article


class IndexView(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['latest_articles'] = Article.objects.all()[:5]
        return context